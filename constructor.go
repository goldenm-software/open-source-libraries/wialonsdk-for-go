package wialon

import (
	"errors"
	"strconv"
)

// New constructor initialize the instance, please send port as 0, host and scheme as empty string ("") if you
// will use Wialon Hosting
func (s *Sdk) New(scheme, host string, port int, isDevelopment bool) error {
	s.isDevelopment = isDevelopment
	s.HasAuthenticated = false
	if s.host == "" {
		s.host = "hst-api.wialon.com"
	} else {
		s.host = host
	}

	if s.scheme == "" {
		s.scheme = "https"
	} else if s.scheme != "http" && s.scheme != "https" {
		return errors.New("Scheme should be http or https")
	} else {
		s.scheme = scheme
	}

	s.baseURL = s.scheme + "://" + s.host

	if s.port < 0 {
		return errors.New("Port cannot be negative")
	} else if s.port > 0 {
		s.port = port
		s.baseURL += ":" + strconv.Itoa(s.port)
	} else {
		s.port = port
	}

	s.baseURL += "/wialon/ajax.html?"

	s.sessionID = ""

	return nil
}
