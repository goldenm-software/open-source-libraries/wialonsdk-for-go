package wialon

// Sdk struct definition for all operations in this library.
// Will store the connecitivty attributes as: host, port and scheme. If you
// want to change this attributes, you need re-initialize the interface.
// Also, will store the sessionID, this attribute will be rewritted or getted
// using GetSessionID or SetSessionID
type Sdk struct {
	sessionID        string // SessionID
	host             string // HTTP host, you should use an IP or an domain
	port             int    // HTTP port (default is 0)
	scheme           string // HTTP scheme (http or https)
	HasAuthenticated bool   // Controlled using login and logout methods.
	baseURL          string // Base URL
	isDevelopment    bool   // Development messages control
}

// SdkResult is the default response of all operations in this library.
type SdkResult struct {
	Method    string      // Method used/called
	Arguments interface{} // Arguments used
	Status    int         // HTTP Status code
	Result    []byte      // JSON result, json.Unmarshal required
}
