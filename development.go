package wialon

import "fmt"

func (s *Sdk) showMessage(method, arguments, url string) {
	fmt.Println("=======================================[ DEVELOPMENT MODE ENABLED ]=======================================")
	fmt.Printf("Method: %s\n", method)
	fmt.Printf("Arguments: %s\n", arguments)
	fmt.Printf("URL processed*: %s\n", url)
	fmt.Println("----------------------------------------------------------------------------------------------------------")
	fmt.Println("* If you need assistance with any error or request, you should contact to Gurtam Support")
	fmt.Println("----------------------------------------------------------------------------------------------------------")
	fmt.Println("Golden M isn't Gurtam, Inc., any issue with Remote API SDK or invalid SDK documentation, please contact")
	fmt.Println("to Gurtam Support")
	fmt.Println("----------------------------------------------------------------------------------------------------------")
	fmt.Println("/* You can see this message because you defined as true the argument isDevelopment in New() */")
	fmt.Println("=======================================[ DEVELOPMENT MODE ENABLED ]=======================================")
}
