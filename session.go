package wialon

import (
	"encoding/json"
	"errors"
)

// GetSessionID is a getter function used to retrieve the sessionID
// used in all SDK requests.
func (s *Sdk) GetSessionID() string {
	return s.sessionID
}

// SetSessionID is a setter function used to set a new sessionID,
// generally you should use this function when you already has a session
func (s *Sdk) SetSessionID(newSessionID string) error {
	if newSessionID == "" {
		return errors.New("SessionID cannot be empty")
	}

	s.HasAuthenticated = true
	s.sessionID = newSessionID
	return nil
}

// Login method, you should use this if you want to login quickly,
// you can find more information about this function in the SDK documentation
// as "token/login" method.
// Disclaimer: method "core/login" was deprecated at 2015.
func (s *Sdk) Login(token string) (SdkResult, error) {
	var args map[string]interface{}
	args = make(map[string]interface{})
	args["token"] = token
	sdkResponse, err := s.Request("token/login", args)
	if err != nil {
		return sdkResponse, err
	}

	if sdkResponse.Status == 500 {
		return SdkResult{}, errors.New("HTTP Error code: 500")
	}

	var resp map[string]interface{}
	json.Unmarshal(sdkResponse.Result, &resp)
	s.sessionID = resp["eid"].(string)
	s.HasAuthenticated = true
	return sdkResponse, err
}

// Logout method, you should use this if you want to logout quickly,
// you can find more information about this function in the SDK documentation
// as "core/logout" method
func (s *Sdk) Logout() (bool, error) {
	var args map[string]interface{}
	args = make(map[string]interface{})
	sdkResponse, err := s.Request("core/logout", args)
	if err != nil {
		return false, err
	}

	if sdkResponse.Status == 500 {
		return false, errors.New("HTTP Error code: 500")
	}

	var resp map[string]interface{}
	json.Unmarshal(sdkResponse.Result, &resp)

	if int(resp["error"].(float64)) == 0 {
		s.sessionID = ""
		return true, err
	}

	s.HasAuthenticated = false
	return false, err
}
