package wialon

import (
	"os"
	"testing"
)

func TestProcedure(t *testing.T) {
	token := os.Getenv("TESTING_TOKEN")
	if token == "" {
		t.Fatal("Expected TESTING_TOKEN environment variable")
	}

	var sdk Sdk
	sdk.New("", "", 0, false)
	_, err := sdk.Login(token)
	if err != nil {
		t.Fatalf("Error handled: %v", err)
	}

	if sdk.GetSessionID() == "" {
		t.Fatal("Session cannot be empty")
	}

	isLoggedOut, err := sdk.Logout()
	if err != nil {
		t.Fatalf("Error handled: %v", err)
	}

	if isLoggedOut {
		t.Log("Logged out successfully")
	} else {
		t.Fatal("Problems logging out")
	}
}
