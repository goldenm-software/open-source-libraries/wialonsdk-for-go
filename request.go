package wialon

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

// Request function to call any function in Wialon SDK, method is defined as "svc"
// in the official documentation, args is "params" argument in the documentation
func (s *Sdk) Request(method string, args interface{}) (SdkResult, error) {
	if s.sessionID == "" && method != "token/login" {
		return SdkResult{}, errors.New("Missing SesisonID, please use Login() to login into Wialon")
	}

	arguments, err := json.Marshal(args)
	if err != nil {
		return SdkResult{}, errors.New("Cannot convert arguments to JSON")
	}

	httpArgs := "svc=" + method + "&params=" + string(arguments) + "&sid=" + s.sessionID

	resp, err := http.Get(s.baseURL + httpArgs)
	if err != nil {
		return SdkResult{}, errors.New("HTTP error: " + err.Error())
	}

	if s.isDevelopment {
		s.showMessage(method, string(arguments), s.baseURL+httpArgs)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return SdkResult{}, errors.New("Failed to read response body: " + err.Error())
	}

	return SdkResult{
		Method:    method,
		Arguments: args,
		Status:    resp.StatusCode,
		Result:    body,
	}, nil
}
