# WialonSDK for Go
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/goldenm-software/open-source-libraries/vuetify-dual-list/blob/master/LICENSE)
[![PkgGoDev](https://pkg.go.dev/badge/gitlab.com/goldenm-software/open-source-libraries/wialonsdk-for-go)](https://pkg.go.dev/gitlab.com/goldenm-software/open-source-libraries/wialonsdk-for-go)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/goldenm-software/open-source-libraries/wialonsdk-for-go)](https://goreportcard.com/report/gitlab.com/goldenm-software/open-source-libraries/wialonsdk-for-go)

## Installation
To install the library, you should use the following command:
```bash
go get gitlab.com/goldenm-software/open-source-libraries/wialonsdk-for-go
```

## Documentation
The documentation is provided by [GoDoc](https://godoc.org/gitlab.com/goldenm-software/open-source-libraries/wialonsdk-for-go)

## Example usage into file
```go
import (
  "fmt"

  "gitlab.com/goldenm-software/open-source-libraries/wialonsdk-for-go"
)

func main() {
  var sdk wialon.Sdk

  scheme := ""
  host := ""
  port := 0
  isDevelopment := true

  sdk.New(scheme, host, port, isDevelopment)
  
  token := "myTokenHere"
	sdkResponse, err := sdk.Login(token)
	if err != nil {
		panic(err)
	}

	var args map[string]interface{}
	args = make(map[string]interface{})
	var spec map[string]interface{}
  spec = make(map[string]interface{})

	spec["itemsType"] = <string>
	spec["propName"] = <string>
	spec["propValueMask"] = <string>
	spec["sortType"] = <string>
	spec["propType"] = <string>

	args["spec"] = spec
	args["force"] = <int>
	args["flags"] = <int>
	args["from"] = <int>
	args["to"] = <int>

	sdkResponse, err := sdk.Request("core/search_items", args)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(sdkResponse.Result))

	hasUnlogged, err = sdk.Logout()
	if err != nil {
		panic(err)
  }
  
  if hasUnlogged {
    fmt.Println("Logged out!")
  } else {
    fmt.Println("Something went wrong, " + err.Error())
  }
}
```

## Main methods
This library supports all types of queries to the [Wialon API](https://sdk.wialon.com/).

## Methods available
For more information, please go to [Wialon Remote API documentation](https://sdk.wialon.com/wiki/en/sidebar/remoteapi/apiref/apiref)

## Work with us!
Feel free to send us an email to [sales@goldenmcorp.com](mailto:sales@goldenmcorp.com)

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.
